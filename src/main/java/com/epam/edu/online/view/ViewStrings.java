package com.epam.edu.online.view;

import com.epam.edu.online.model.TextUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class ViewStrings {
    private static final Logger log = LogManager.getLogger(ViewStrings.class);

    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methodMainMenu = new LinkedHashMap<>();
    private Map<String, String> menuLanguages = new LinkedHashMap<>();
    public static Scanner input = new Scanner(System.in);
    private TextUtil textUtil = new TextUtil();

    private ResourceBundle bundle;
    private ResourceBundle bundleList;
    private static ResourceBundle bundleWords;

    public ViewStrings() {
        this.bundle = ResourceBundle.getBundle("menu", new Locale("en"));
        this.bundleList = ResourceBundle.getBundle("list_languages");
        bundleWords = ResourceBundle.getBundle("words", new Locale("en"));

        fillMenu();
        fillMethodMenu();
        fillMenuLanguages();
        printMenu();

    }

    private void printMenu() {
        menu.forEach((key, value) -> System.out.println("`"+key+"` - "+"\t"+value));
        log.info("\n" + bundleWords.getString("choosecommand"));
    }

    private void printListLanguages(){
        menuLanguages.forEach((key, value) -> System.out.println("`"+key+"` - "+"\t"+value));
    }

    private void selectEnLanguage() {
        this.bundle = ResourceBundle.getBundle("menu", new Locale("en"));
        bundleWords = ResourceBundle.getBundle("words", new Locale("en"));
        fillMenu();
        printMenu();
    }
    private void selectUkLanguage() {
        this.bundle = ResourceBundle.getBundle("menu", new Locale("uk"));
        bundleWords = ResourceBundle.getBundle("words", new Locale("uk"));
        fillMenu();
        printMenu();
    }

    private void fillMenu() {
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("q", bundle.getString("q"));
    }

    private void fillMenuLanguages() {
        menuLanguages.put("en", bundleList.getString("1"));
        menuLanguages.put("uk", bundleList.getString("2"));
    }


    private void fillMethodMenu() {
        methodMainMenu.put("1", this::printListLanguages);
        methodMainMenu.put("2", this::printMenu);
        methodMainMenu.put("en", this::selectEnLanguage);
        methodMainMenu.put("uk", this::selectUkLanguage);
        methodMainMenu.put("3", ()-> textUtil.findTheLargestNumberSentencesTheSameWords());
        methodMainMenu.put("4", ()-> textUtil.printSentencesOrderByCountWords());
        methodMainMenu.put("5", ()-> textUtil.uniqueWordInFirstSentence());
        methodMainMenu.put("6", ()-> textUtil.printUniqueWordInQuestionSentences());
        methodMainMenu.put("7", ()-> textUtil.vowelWordChangeToTheLargestWord());
        methodMainMenu.put("q", () -> System.out.println(bundleWords.getString("by")));
    }

    public static String bundleWord(String key) {
        return bundleWords.getString(key);
    }

    public void run() {
        String input = "";
        do {
            try {
                input = ViewStrings.input.next().toLowerCase();
                methodMainMenu.get(input).print();
            } catch (NullPointerException e) {
                log.error(bundleWords.getString("wrongcommand"));
                printMenu();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }

}

