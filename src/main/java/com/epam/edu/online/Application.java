package com.epam.edu.online;

import com.epam.edu.online.view.ViewStrings;

public class Application {

    public static void main(String[] args) {
        new ViewStrings().run();
    }

}
