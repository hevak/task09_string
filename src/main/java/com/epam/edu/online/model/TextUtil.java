package com.epam.edu.online.model;

import com.epam.edu.online.view.ViewStrings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtil {
    private static final Logger log = LogManager.getLogger(TextUtil.class);

    private String text;
    private List<Sentence> sentences = new ArrayList<>();

    public TextUtil() {
        try {
            this.text = TextReader.readText("etc/text.txt");
            for (String sentence : text.trim().split("[.!?]")) {
                this.sentences.add(new Sentence(sentence));
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void vowelWordChangeToTheLargestWord() {
        for (Sentence sentence : sentences) {
            log.info(ViewStrings.bundleWord("old") + sentence.getText());
            Word theLargerWord = getTheLargestWord(sentence);
            Word vowelWord = getVowelWord(sentence);
            Sentence newSentence;
            if (vowelWord != null) {
                log.info(theLargerWord.getText());
                log.info(vowelWord.getText());
                newSentence = new Sentence(sentence.getText().replace(vowelWord.getText(), theLargerWord.getText()));
                log.info(ViewStrings.bundleWord("new") + newSentence.getText());
            } else {
                log.info(ViewStrings.bundleWord("nonevowel"));
                log.info(ViewStrings.bundleWord("new") + sentence.getText());
            }
        }
    }

    private Word getVowelWord(Sentence sentence) {
        Pattern p = Pattern.compile("^[аеиіоуaeiouy].*", Pattern.CASE_INSENSITIVE);
        for (Word word : sentence.getWords()) {
            Matcher matcher = p.matcher(word.getText());
            if (matcher.find()) {
                return new Word(matcher.group());
            }
        }
        return null;
    }

    private Word getTheLargestWord(Sentence sentence) {
        Word theLargerWord = new Word("");
        for (Word word : sentence.getWords()) {
            if (word.getText().length() > theLargerWord.getText().length()) {
                theLargerWord = word;
            }
        }
        return theLargerWord;
    }

    public void printUniqueWordInQuestionSentences() {
        Pattern pattern = Pattern.compile("([^.?!]*)\\?");
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            Sentence questionSentence = new Sentence(matcher.group());
            log.info(questionSentence);
            log.info(ViewStrings.bundleWord("entersize"));
            printWordsBySize(questionSentence, ViewStrings.input.nextInt());
        }
    }

    private void printWordsBySize(Sentence sentence, int size) {
        int counter = 0;
        Set<Word> words = new HashSet(sentence.getWords());
        for (Word word : words) {
            if (word.getText().length() == size) {
                log.info(word);
                counter++;
            }
        }
        if (counter == 0) {
            log.info(ViewStrings.bundleWord("empty"));
        }
    }

    public void uniqueWordInFirstSentence() {
        List<Word> firstSentenceWordsSet = new ArrayList<>(sentences.get(0).getWords());
        for (int i = 1; i < sentences.size(); i++) {
            for (Word word : sentences.get(i).getWords()) {
                firstSentenceWordsSet.remove(word);
            }
        }
        firstSentenceWordsSet.forEach(word -> log.info(word.getText()));
    }

    public void printSentencesOrderByCountWords() {
        Map<Integer, Integer> countWordInSentences = getCountWordInSentences();
        countWordInSentences.entrySet()
                .stream()
                .sorted(Comparator.comparingInt(Map.Entry::getKey))
                .forEach((map) -> log.info(map.getKey() + " "
                        + ViewStrings.bundleWord("words")
                        + "\t\t"
                        + map.getValue() + " "
                        + ViewStrings.bundleWord("sentence")));
    }

    private Map<Integer, Integer> getCountWordInSentences() {
        Map<Integer, Integer> countWordInSentences = new LinkedHashMap<>();
        for (int i = 1; i < sentences.size(); i++) {
            countWordInSentences.put(sentences.get(i).getWords().size(), i);
        }
        return countWordInSentences;
    }

    public void findTheLargestNumberSentencesTheSameWords() {
        List<Map<Word, Integer>> listWordsCount = new ArrayList<>();
        Map<Word, Integer> wordCount;
        for (Sentence sentence : sentences) {
            wordCount = new LinkedHashMap<>();
            for (Word word : sentence.getWords()) {
                if (word.getText().length() != 0) {
                    //compute()
                    Integer integer = wordCount.get(word);
                    wordCount.put(word, integer == null ? 1 : integer + 1);
                }
            }
            listWordsCount.add(wordCount);
        }
        getMaxWordEachSentence(listWordsCount);
    }


    private void getMaxWordEachSentence(List<Map<Word, Integer>> listWordsCount) {
        listWordsCount.forEach(map->{
            Map.Entry<Word, Integer> stringIntegerEntry = map.entrySet().stream()
                    .max(Comparator.comparing(Map.Entry::getValue))
                    .orElse(null);
            log.info(stringIntegerEntry.getKey().getText()
                    + " - " + stringIntegerEntry.getValue());
        });
    }


}
