package com.epam.edu.online.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sentence {

    private static final Logger log = LogManager.getLogger(Sentence.class);
    private String text;
    private List<Word> words = new ArrayList<>();


    public Sentence(String text) {
        this.text = clearSentence(text);
        toListWords(this.text);
    }

    public String getText() {
        return text;
    }

    public List<Word> getWords() {
        return words;
    }

    private String clearSentence(String sentence) {
        sentence = sentence.replaceAll("[():;!?,=—]", "");
        sentence = sentence.replaceAll("[/]", " ");
        return sentence;
    }

    private void toListWords(String text) {
        for (String wordText : text.split("[\\s]+")) {
            String word = wordText.trim();
            if (word.length() > 0) {
                this.words.add(new Word(word));
            }
        }
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "text='" + text + '\'' +
                '}';
    }
}
